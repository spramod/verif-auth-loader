# Authenticated Loader Model and Verification

This directory contains the models used in the experiments for our FMCAD 2019 paper entitled [Verification of Authenticated Firmware Loaders](https://eprint.iacr.org/2019/564.pdf). The model here reads in an image from the flash device, copies this data into memory, computes a cryptographic digest over this memory region and checks whether this digest value matches the expected value.

## UCLID5

The models are written in the open source UCLID5 verification and synthesis language. You can [download UCLID5 from here](https://github.com/uclid-org/uclid).

## Files

1. common.ucl: This contains the type definitions of addr_t and data_t. You can change the bitwidth used in the other models in this file.
2. hashcheck.ucl: This contains the actual model.
3. prop5.ucl: This checks property 5 (no bypassing).
4. prop6.ucl: This checks property 6 (no TOCTOU).
5. prop7a.ucl: This checks property 7a which states that digest_value is equal to the hash of the memory region between load_addr and load_addr + data_index.
6. prop7b.ucl: This checks property 7b which states that in the absence of adversary tampering the contents of memory and flash (input device) are the same.
7. prop7c.ucl: This checks property 7c, which states that whenever the property is verified, the hash of the memory region has the expected value.  8. mem_digest_lemma.ucl: This proves the auxiliary lemma (mem_eq_impl_digest_eq) that is assumed in 7a and 7b.

## Commands

Use the following command to run each of the models.

    $ for i in 5 6 7a 7b 7c; do time uclid common.ucl hashcopy.ucl prop$i.ucl; done; uclid common.ucl mem_digest_lemma.ucl

Sample output is shown here.

    Successfully parsed 3 and instantiated 1 module(s).
    50 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

    real	0m7.507s
    user	0m8.376s
    sys	0m0.204s
    Successfully parsed 3 and instantiated 1 module(s).
    50 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

    real	0m5.320s
    user	0m9.272s
    sys	0m0.186s
    Successfully parsed 3 and instantiated 1 module(s).
    21 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

    real	0m8.449s
    user	0m12.110s
    sys	0m0.192s
    Successfully parsed 3 and instantiated 1 module(s).
    21 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

    real	0m4.303s
    user	0m7.623s
    sys	0m0.221s
    Successfully parsed 3 and instantiated 1 module(s).
    25 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

    real	0m4.044s
    user	0m7.901s
    sys	0m0.214s
    Successfully parsed 2 and instantiated 1 module(s).
    12 assertions passed.
    0 assertions failed.
    0 assertions indeterminate.
    Finished execution for module: main.

## Notes

1. <_u, <=_u, >_u etc. refer to the unsigned variants of the bit vector comparison operators.
2. We had to fiddle with the SMT solver (Z3) configuration options for the proofs to go through. Please see the set_solver_option command in the control block and try changing the values of :smt.RELEVANCY and :smt.CASE_SPLIT from 0 to 1 (or vice versa). It may also help to disable array extensionality, as is done in prop7c.ucl.

## Contact

Please do contact us if you have any questions/comments about the model or the paper.

* Sujit Kumar Muduli (smuduli at cse iitk ac in)
* Pramod Subramanyan (spramod at cse iitk ac in)
* Sayak Ray          (sayak ray at intel com)

## Reference

If you build upon or use these models in paper of your own, it is suggested that you cite the following FMCAD 2019 paper.

[Verification of Authenticated Firmware Loaders](https://eprint.iacr.org/2019/564.pdf).    
[Sujit Kumar Muduli](https://www.cse.iitk.ac.in/users/smuduli/), [Pramod Subramanyan](https://www.cse.iitk.ac.in/users/spramod/) and Sayak Ray.    
Proceedings of Formal Methods in Computer-Aided Design (FMCAD) 2019.
