module main
{
  type addr_t = common.addr_t;
  type state_t = soc.state_t;

  const image_sz : addr_t;
  const image_base_addr1, image_base_addr2 : addr_t;
  const image_load_addr1, image_load_addr2 : addr_t;

  input t, tnop : boolean;

  instance soc1 : soc(tamper_step : (t), tamper_nop : (tnop));
  instance soc2 : soc(tamper_step : (t), tamper_nop : (true));

  init {
    assume image_base_addr1 == soc1.image_base_addr;
    assume image_base_addr2 == soc2.image_base_addr;
    assume image_base_addr1 + image_sz >_u image_base_addr1;
    assume image_base_addr2 + image_sz >_u image_base_addr2;
    assume image_load_addr1 + image_sz >_u image_load_addr1;
    assume image_load_addr2 + image_sz >_u image_load_addr2;
    assume image_load_addr1 == soc1.image_load_addr;
    assume image_load_addr2 == soc2.image_load_addr;

    assume 
      forall (a : addr_t) ::
        (a <_u image_sz) ==> 
          (soc1.flash[image_base_addr1 + a] == soc2.flash[image_base_addr2 + a]);
    assume soc1.image_sz == image_sz;
    assume soc2.image_sz == image_sz;
    assume soc1.expected_digest_value == soc2.expected_digest_value;
    assume soc1.hash_base == soc2.hash_base;
  }
  
  next {
    next (soc1);
    next (soc2);
  }

  axiom hash_unforgeable: soc1.tmon ==> 
        soc.mem_digest(soc1.mem, image_load_addr1, image_sz) != soc1.expected_digest_value;

  invariant flash_eq: 
    forall (a : addr_t) ::
      (a <_u image_sz) ==> 
        (soc1.flash[image_base_addr1 + a] == soc2.flash[image_base_addr2 + a]);
  invariant mem_eq_flash_1: !soc1.tmon ==>
    (forall (a : addr_t) ::
      (a <_u (if (soc1.state == ST_COPY) 
                then soc1.data_index
                else image_sz)) ==>
        (soc1.mem[image_load_addr1 + a] == soc1.flash[image_base_addr1 + a]));
  invariant mem_eq_flash_2:
    (forall (a : addr_t) ::
      (a <_u (if (soc2.state == ST_COPY) 
                then soc2.data_index
                else image_sz)) ==>
        (soc2.mem[image_load_addr2 + a] == soc2.flash[image_base_addr2 + a]));
  invariant mem_eq:
    (forall (a : addr_t) :: !soc1.tmon ==>
      (a <_u (if (soc1.state == ST_COPY) 
                then soc1.data_index
                else image_sz)) ==>
        (soc1.mem[image_load_addr1 + a] == soc2.mem[image_load_addr2 + a]));

  invariant not_tampered: !soc2.tmon;
  invariant sz1_eq: soc1.image_sz == image_sz;
  invariant sz2_eq: soc2.image_sz == image_sz;
  invariant addr1_eq: soc1.image_load_addr == image_load_addr1;
  invariant addr2_eq: soc2.image_load_addr == image_load_addr2;
  invariant exp_eq: soc1.expected_digest_value == soc2.expected_digest_value;
  invariant hbase_eq: soc1.hash_base == soc2.hash_base;
  invariant cnt_range1a: soc1.data_index <=_u image_sz;
  invariant cnt_range2a: soc2.data_index <=_u image_sz;
  invariant cnt_range1b: (soc1.state == ST_COPY) ==> soc1.data_index <_u image_sz;
  invariant cnt_range2b: (soc2.state == ST_COPY) ==> soc2.data_index <_u image_sz;
  invariant nowrap1a: image_base_addr1 + image_sz >_u image_base_addr1;
  invariant nowrap1b: image_load_addr1 + image_sz >_u image_load_addr1;
  invariant nowrap2a: image_base_addr2 + image_sz >_u image_base_addr2;
  invariant nowrap2b: image_load_addr2 + image_sz >_u image_load_addr2;
  invariant hval_eq: (!soc1.tmon || soc1.state == ST_COPY) ==> 
                        (soc1.digest_value == soc2.digest_value);
  invariant cnt_eq: soc1.data_index == soc2.data_index;
  invariant st_eq: (soc1.state == soc2.state)                             ||
                   (soc1.state == ST_FAILED && soc2.state == ST_VERIFIED) ||
                   (soc2.state == ST_FAILED && soc1.state == ST_VERIFIED);
  invariant hash_so_far_0:
    soc1.state == ST_COPY ==> soc1.digest_value == soc1.hash_base;
  assume hash_so_far_n: 
    soc1.state == ST_COMPUTE_HASH ==> 
      soc1.digest_value == soc.mem_digest(soc1.mem, soc1.image_load_addr, soc1.data_index);
  property verif_impl: soc1.state == ST_VERIFIED ==>
                       soc2.state == ST_VERIFIED;

  control {
    set_solver_option(":smt.RELEVANCY", 0);
    set_solver_option(":smt.CASE_SPLIT", 0);
    v = induction(1);
    check;
    print_results;
    /*
    v.print_cex(soc1.mem, soc2.mem, 
                soc1.state, soc2.state,
                soc1.image_load_addr, soc2.image_load_addr,
                soc1.image_sz, soc2.image_sz,
                soc1.data_index, soc2.data_index);
    */
  }
}
